# bdm-mrocs-sdk

MROCS SDK to make restitution based on b-eden API
Please refer to the web API documentation **b-eden - API v1 Report** and **b-eden - API v1 Module**.


## API

* `get`
  * available actions
* `dimensions`
  * send content width and height to parent


## `get`

```javascript
function get(action: Enum, options: Object): Promise<DataObject>
```

get data from b-eden api.


#### `action` argument

_(Enum)_: required

Refers to `:endpoint` in mentioned web API documentation

An enum list is available in `ACTION_LIST` object


#### `options` argument

_(Object)_: optional

Formatted as follows

```javascript
{
  offset: Number? = undefined,
  filter: Object? = {},
}
```


#### `Return`

`get` return a `promise` resolved with `DataObject`.

Please refer to the web API documentation **b-eden - API v1 Report** and **b-eden - API v1 Module** to determine DataObject properties for each method.


#### Usage

````javascript
import { get } from '@b-flower/bdm-mrocs-sdk'

... //your code
get('report/participation')
  .then(dataObject => {
    // do something
  })
...

````


#### `action list`
````javascript
import { ACTION_LIST } from '@b-flower/bdm-mrocs-sdk'
````

  * Available action list
    * **PARTICIPATION**: 'report/participation',
    * **RESULTS**: 'report/results',
    * **LEARNERS_COUNT**: 'report/learners/count',
    * **LEARNERS**: 'report/learners',
    * **INTERACTIONS_COUNT**: 'report/interactions/count',
    * **INTERACTIONS**: 'report/interactions',
    * **INTERACTIONS_LIST**: 'report/interactions/list',
    * **MOD_INFO**: 'module/info',
    * **MOD_INTERACTIONS**: 'module/interactions',


#### Usage with action list

````javascript
import { get, ACTION_LIST } from '@b-flower/bdm-mrocs-sdk'

... //your code
get(ACTION_LIST.PARTICIPATION)
  .then(dataObject => {
    // do something
  })
...

````


#### Usage with react integration

````javascript
import React from 'react'

import { get, ACTION_LIST } from '@b-flower/bdm-mrocs-sdk'

class Result extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      result: {}
    }
  }

  componentWillMount() {
    get(ACTION_LIST.RESULTS)
      .then(r => {
        this.setState({ result: r })
      })
  }

  render() {
    return (
      <div>
        <pre>
          {JSON.stringify(this.state.result )}
        </pre>
      </div>
    )
  }
}

export default Result

````


## `dimensions`

```javascript
function dimensions({ width: Number, height: Number }): void
```

Send inner content dimensions to b-eden

#### Usage (example)
```
  componentDidMount() {
    this.props.sdk.dimensions({
      width: myRef.clientWidth,
      height: myRef.clientHeight,
    })
  }
```
