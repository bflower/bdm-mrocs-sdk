// import { ERR_UNKNOWN_ACTION } from './error_codes'
export const ACTION_LIST = {
  PARTICIPATION: 'report/participation',
  RESULTS: 'report/results',
  LEARNERS_COUNT: 'report/learners/count',
  LEARNERS: 'report/learners',
  INTERACTIONS_COUNT: 'report/interactions/count',
  INTERACTIONS: 'report/interactions',
  INTERACTIONS_LIST: 'report/interactions/list',
  MOD_INFO: 'module/info',
  MOD_INTERACTIONS: 'module/interactions',
}

