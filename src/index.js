import Promise from 'bluebird'
import Coachee, {
  ERR_CONNECTION_DESTROYED,
  ERR_CONNECTION_TIMEOUT,
  ERR_NOT_IN_IFRAME,
} from 'coachee'

import { ACTION_LIST } from './actions'

Coachee.Promise = Promise

const API_VERSION = 'v1'
const API_VERSION_KEY = 'apiVersion'

// store current connection to parent and avoid unecessary reconnection
// it seems that multiple connectToParent make some noise on parent receiver
let connection

function connect() {
  return Promise.try(() => {
    // in try because connectToParent may throw
    if (!connection) connection = Coachee.connectToParent({})
    return connection
  })
}

function getConnectionPromise() {
  return connect().then(connection => connection.promise)
}

function getConnectionDestroy() {
  return connect().then(connection => connection.destroy)
}

function ensureApiVersion(options) {
  return Object.assign({}, options, { [API_VERSION_KEY]: API_VERSION })
}

function get(action, options) {
  return getConnectionPromise().then(parent =>
    parent.get(action, ensureApiVersion(options))
  )
}

function dimensions({ width, height }, options) {
  return getConnectionPromise().then(parent =>
    parent.dimensions({ width, height }, ensureApiVersion(options))
  )
}

/**
 * destroy should be not necessary on child as child context is destroyed
 * when iframe is removed.
 * It is not documented yet.
 */
function destroy() {
  return getConnectionDestroy().then(destroy => destroy())
}

module.exports = {
  ACTION_LIST,

  ERR_NOT_IN_IFRAME,
  ERR_CONNECTION_DESTROYED,
  ERR_CONNECTION_TIMEOUT,

  get,
  dimensions,
  destroy
}
